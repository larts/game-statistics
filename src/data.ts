import { Feature as OLFeature } from 'ol'
import Collection from 'ol/Collection'
import OLGeometry from 'ol/geom/Geometry'
import OLPoint from 'ol/geom/Point'
import OLPolygon from 'ol/geom/Polygon'

const PERIMETER_RAW = require('./perimeter.json')
const POIS_RAW: PoiRaw[] = require('./POIs.json')

export const perimeterLine = new OLFeature(new OLPolygon([PERIMETER_RAW]).transform('EPSG:4326', 'EPSG:3857'))


interface PoiRaw {
    id: number
    type: 'other' | 'food' | 'wood' | 'iron' | 'castle'
    epsg4326: [number, number]
    color?: string
}

export type PoiProperties =
    | PoiProperties.Castle
    | PoiProperties.Camp
export namespace PoiProperties {
    interface Base {
        type: 'leather' | 'food' | 'wood' | 'iron' | 'castle'
        color?: string
        zone: OLGeometry
        circle: OLGeometry
        colors: Array<{
            color: string
            weight: number
            distance: number
        }>
        bubbleCollect: OLGeometry
        bubbleLoot: OLGeometry
        bubble: null | {
            team: string
            amount: number
            color: string
            offset: number
            opacity: number
        }
    }
    export interface Castle extends Base {
        type: 'castle'
        proximity: number
    }
    export interface Camp extends Base {
        type: 'leather' | 'food' | 'wood' | 'iron'
    }
}


export const POIs = new Collection(POIS_RAW.map(({id, epsg4326, ...properties}, i) => {
        const point = new OLPoint(epsg4326).transform('EPSG:4326', 'EPSG:3857')

        const feature = new OLFeature(point)
        feature.setId(id);
        feature.setProperties({
            ...properties,
            zone: null,
            circle: null,
        })

        return feature
    })
)
export const castles = new Collection(POIs.getArray().filter((poi) => poi.getProperties().type.includes('castle')))
