
declare module 'polygon-splitter' {
    export default function polygonSplitter(
        polygon: import('@turf/turf').Polygon,
        line: import('@turf/turf').LineString,
    ): import('@turf/turf').Polygon | import('@turf/turf').Feature<import('@turf/turf').MultiPolygon>
}

declare module '*.tsv' {
    const url: string
    export default url
}
