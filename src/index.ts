import './export'
import 'ol/ol.css'

import * as turf from '@turf/turf'
import FillPattern from 'ol-ext/style/FillPattern'
import Collection from 'ol/Collection'
import { defaults as defaultControls } from 'ol/control'
import MousePosition from 'ol/control/MousePosition'
import { CoordinateFormat } from 'ol/coordinate'
import { getCenter } from 'ol/extent'
import Feature, { FeatureLike } from 'ol/Feature'
import Polygon from 'ol/geom/Polygon'
import SimpleGeometry from 'ol/geom/SimpleGeometry'
import { Modify } from 'ol/interaction'
import { Vector as VectorLayer } from 'ol/layer'
import BaseLayer from 'ol/layer/Base'
import ImageLayer from 'ol/layer/Image'
import TileLayer from 'ol/layer/Tile'
import Map from 'ol/Map'
import { transform, transformExtent } from 'ol/proj'
import { OSM, Vector as VectorSource } from 'ol/source'
import ImageStatic from 'ol/source/ImageStatic'
import { Fill, Icon, Stroke, Style, Text } from 'ol/style'
import View from 'ol/View'

import { castles, perimeterLine, PoiProperties, POIs } from './data'
import { updateCircle, updateFill, updateProximity, updateZone } from './vendor'
import { updateTimelapse } from './timelapse'

const territoryLilaste = [
    24.3430, 57.16848,  // kreisi, apaksa
    24.3675, 57.17894,  // labi, augsa
]
const territorySilciems = [
    24.7854, 57.1161 ,  // kreisi, apaksa
    24.8028, 57.12672,  // labi, augsa
]
const territorySuntazi2 = [
    24.9607, 56.9234,  // kreisi, apaksa
    24.9708, 56.9280,  // labi, augsa
]
console.log(getCenter(territorySuntazi2))


const icons = {
    leather: './LEATHER.svg',
    market: './MARKET.svg',
    food_honey: './F_HONEY.svg',
    food_lavender: './F_LAVENDER.svg',
    food_cucumber: './F_CUCUMBER.svg',
    food_mushroom: './F_MUSHROOM.svg',
    food_tomato: './F_TOMATO.svg',
    food_raspberry: './F_RASPBERRY.svg',
    wood: './WOOD.svg',
    iron: './IRON.svg',
    castle1: './CASTLE1.svg',
    castle2: './CASTLE2.svg',
    castle3: './CASTLE3.svg',
    castle4: './CASTLE4.svg',
    castle5: './CASTLE5.svg',
    castle6: './CASTLE6.svg',
    castle7: './CASTLE7.svg',
}

var pointStyleFunction = (feature: FeatureLike, resolution: number) => {
    const id = feature.getId();
    const properties = (feature.getProperties() as PoiProperties)
    return new Style({
        // image: new Circle({
        //     radius: properties.type === 'castle' ? 18 : 12,
        //     stroke: new Stroke({
        //         color: '#fff',
        //         width: 2,
        //     }),
        //     fill: new Fill({
        //         color: properties.type === 'castle' ? properties.color : colors[properties.type],
        //     })
        // }),
        image: new Icon({
            src: icons[properties.type],
            scale: 1.5 / resolution,
        }),
        text: new Text({
            text: id === 99 ? '' : `${id}`,
            scale: 1.5 / resolution,
            offsetY: 24 / resolution,
            offsetX: 0,
            backgroundFill: new Fill({
                color: '#BFE0A1dd',
            }),
        }),
    })
}

var bubbleStyleFunction = (feature: FeatureLike, resolution: number) => {
    const properties = (feature.getProperties() as PoiProperties)
    const bubble = properties.bubble

    if(!bubble) return new Style({
        stroke: new Stroke({
            color: `rgba(0, 0, 0, 0)`,
            width: 2,
        }),
        fill: new Fill({
            color: `rgba(0, 0, 0, 0)`,
        }),
    })
    const opacityHex = Math.round(bubble.opacity * 255).toString(16).padStart(2, '0')

    return new Style({
        // image: new Circle({
        //     radius: properties.type === 'castle' ? 18 : 12,
        //     stroke: new Stroke({
        //         color: '#fff',
        //         width: 2,
        //     }),
        //     fill: new Fill({
        //         color: properties.type === 'castle' ? properties.color : colors[properties.type],
        //     })
        // }),
        stroke: new Stroke({
            color: `rgba(0, 0, 0, ${bubble.opacity})`,
            width: 3,
        }),
        geometry: (feature: Feature) => {
            const cloned = (bubble.amount <= 2 ? properties.bubbleLoot : properties.bubbleCollect).clone()
            cloned.translate(0, (1 - bubble.opacity) * 96)
            return cloned
        },
        fill: new Fill({
            color: `${bubble.color}${opacityHex}`,
        }),
        text: new Text({
            scale: 1,
            fill: new Fill({
                color: `#000000${opacityHex}`,
            }),
            font: 'bold 24px Arial, Verdana, Helvetica, sans-serif',
            text: `${bubble.amount}`,
        }),
    })
}

var circleStyleFunction = (feature: FeatureLike, resolution: number) => {
    const properties = (feature.getProperties() as PoiProperties)
    if(!properties.type.includes('castle')) return []

    return [
        new Style({
            stroke: new Stroke({
                color: 'rgba(0, 0, 0, 0.6)',
                width: 2,
            }),
            geometry: (feature: Feature) => properties.circle,
        })
    ]
}

export const zoneStyleFunction = (feature: FeatureLike, resolution: number) => {
    const properties = (feature.getProperties() as PoiProperties)

    const styles = [
        new Style({
            stroke: new Stroke({
                color: 'rgba(0, 0, 0, 0.6)',
                width: 2,
            }),
            geometry: (feature: Feature) => properties.zone,
        })
    ]

    if(properties.type === 'leather') return styles

    if(properties.type.includes('castle')) {
        styles.push(
            new Style({
                fill: new Fill({
                    color: properties.colors[0].color,
                }),
                geometry: (feature: Feature) => properties.zone,
            })
        )
        return styles
    }

    const spacing = 10
    const scale = 1

    let offset = 0
    properties.colors?.forEach(({color, weight}) => {
        const size = spacing * weight
        styles.push(
            new Style({
                fill: new FillPattern({
                    pattern: 'hatch',
                    color,
                    size,
                    spacing,
                    angle: 0,
                    offset: offset + size / 2,
                    scale,
                }),
                geometry: (feature: Feature) => properties.zone,
            })
        )
        offset = offset + size
    })

    return styles
}

;
(async () => {
    var source = new VectorSource({wrapX: false});
    const zonesSource = new VectorSource({features: POIs})

    // Check that distance looks the same across NS and WE
    const polygonPerimeter = turf.polygon((perimeterLine.getGeometry().clone().transform('EPSG:3857', 'EPSG:4326') as SimpleGeometry).getCoordinates());
    const center = turf.center(polygonPerimeter)
    const north = turf.destination(center, 200, 0, {units: 'meters'})
    const northEast = turf.destination(north, 200, 90, {units: 'meters'})
    const east = turf.destination(northEast, 200, 180, {units: 'meters'})
    const square = new Polygon([[center, north, northEast, east, center].map((feature) => feature.geometry.coordinates)]).transform('EPSG:4326', 'EPSG:3857')

    const layers = [
        new TileLayer({
            source: new OSM(),
        }),
        new ImageLayer({
            source: new ImageStatic({
                imageExtent: territorySuntazi2,
                projection: 'EPSG:4326',
                url: './suntazi2.png',
            }),
            opacity: 0.999,
        }),
        new VectorLayer({
            source: zonesSource,
            style: zoneStyleFunction,
            opacity: 0.6,
        }),
        new VectorLayer({
            source: zonesSource,
            style: circleStyleFunction,
            opacity: 0,
        }),
        new VectorLayer({
            source: zonesSource,
            style: pointStyleFunction,
            opacity: 1,
        }),
        new VectorLayer({
            source: zonesSource,
            style: bubbleStyleFunction,
            opacity: 1,
        }),
        new VectorLayer({
            source: new VectorSource({features: [perimeterLine]}),
            style: new Style({
                stroke: new Stroke({
                    color: 'rgba(0, 0, 0, 255)',
                    width: 4,
                }),
            }),
            opacity: 0,
        }),
        new VectorLayer({
            source: new VectorSource({features: [new Feature(square)]}),
            style: new Style({
                stroke: new Stroke({
                    color: 'rgba(0, 0, 0, 255)',
                    width: 16,
                }),
            }),
            opacity: 0,
        }),
        new VectorLayer({
            source: source,
        }),
    ] as BaseLayer[]


    const coordinateFormat: CoordinateFormat = ([x, y]) => {
        const epsg3857 = transform([x, y], 'EPSG:4326', 'EPSG:3857')
        return [
            `Lat.Long.(EPSG:4326): ${x.toFixed(5)}, ${y.toFixed(5)}`,
            `Lat.Long.(EPSG:3857): ${epsg3857[0].toFixed(0)}, ${epsg3857[1].toFixed(0)}`,
        ].join('<br/>')
    }
    // https://openlayers.org/en/latest/examples/mouse-position.html?q=mouse
    const mousePositionControlLatLong = new MousePosition({
        className: 'custom-mouse-position-latlong',
        coordinateFormat: coordinateFormat,
        projection: 'EPSG:4326',
        target: document.getElementById('mouse-position-latlong'),
        undefinedHTML: '&nbsp;',
    })


    const refresh = () => {
        POIs.getArray().forEach(updateCircle)
        POIs.getArray().forEach(updateZone)
        POIs.getArray().forEach(updateFill)
        updateTimelapse(Number((document.getElementById('tick') as HTMLInputElement).value ?? 0))
        // POIs.getArray().forEach(updateProximity)
        // const count = castles.getArray().length
        // const sum = castles.getArray().reduce((sum, castle) => sum + Number((castle.getProperties() as PoiProperties.Castle).proximity.toFixed(2)), 0)
        // console.info(`Total proximity (${sum.toFixed(2)}): \n`+castles.getArray().map((castle) => {
        //     const proximityRelative = Number((castle.getProperties() as PoiProperties.Castle).proximity.toFixed(2)) / (sum / count)
        //     return `${castle.getId()}: ${(proximityRelative*100).toFixed().padStart(3)}% ${'-'.repeat(Math.max(0, Math.min(20, proximityRelative * 100 - 80)))}${'+'.repeat(Math.max(0, proximityRelative * 100 - 100))}`
        // }).join('\n'))
    }
    refresh()

    const modifyPOIs = new Modify({
        features: POIs
    });
    modifyPOIs.on(['modifyend'], refresh);

    const modifyPerimeter = new Modify({
        features: new Collection([perimeterLine]),
    });
    modifyPerimeter.on(['modifyend'], function (event) {
        POIs.getArray().forEach(updateZone)
    });

    new Map({
        controls: defaultControls().extend([
            // mousePositionControlLatLong,
        ]),
        layers,
        target: 'map',
        view: new View({
            center: transform(getCenter(territorySuntazi2), 'EPSG:4326', 'EPSG:3857'),
            extent: transformExtent(territorySuntazi2, 'EPSG:4326', 'EPSG:3857'),
            showFullExtent: true,
            zoom: 17,
            maxZoom: 18,
        }),
        // interactions: [
        //     modifyPOIs,
        //     modifyPerimeter,
        // ]
    })

    console.info('Done!')
})()
    .catch((err) => console.error(err))

