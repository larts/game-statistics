import { PoiProperties, POIs } from './data'
import tsv1 from './timelapse-data/2023-08-11 Throne X/1.tsv'
import tsv10 from './timelapse-data/2023-08-11 Throne X/10.tsv'
import tsv11 from './timelapse-data/2023-08-11 Throne X/11.tsv'
import tsv13 from './timelapse-data/2023-08-11 Throne X/13.tsv'
import tsv16 from './timelapse-data/2023-08-11 Throne X/16.tsv'
import tsv19 from './timelapse-data/2023-08-11 Throne X/19.tsv'
import tsv2 from './timelapse-data/2023-08-11 Throne X/2.tsv'
import tsv3 from './timelapse-data/2023-08-11 Throne X/3.tsv'
import tsv5 from './timelapse-data/2023-08-11 Throne X/5.tsv'
import tsv6 from './timelapse-data/2023-08-11 Throne X/6.tsv'
import tsv7 from './timelapse-data/2023-08-11 Throne X/7.tsv'
import tsv8 from './timelapse-data/2023-08-11 Throne X/8.tsv'
import tsv9 from './timelapse-data/2023-08-11 Throne X/9.tsv'


enum CampEventId {
    Capture = 0,
    Collect = 1,
    Loot = 2,
    Upgrade = 3,
    SetStockpile = 4,
    StartCapture = 5,
    CancelCapture = 6,
    SetStartingCollectCooldown = 7,
}
enum TeamId {
    Neutral = 'NEUTRAL',
    Bears = 'BEARS',
    Lynx = 'LYNX',
    Snakes = 'SNAKES',
}
type ExportRaw = [heartbeat: number, event: CampEventId, team: TeamId, value: number]
interface CampState {
    timestamp: number
    owner: TeamId
    contester: TeamId | null
    collected: Array<{timestamp: number; team: TeamId; amount: number}>
    looted: Array<{timestamp: number; team: TeamId; amount: number}>
    upgraded: Array<{timestamp: number; team: TeamId}>
}

const logToStates = (logs: ExportRaw[]) => {
    const states: CampState[] = [{
        timestamp: 0,
        owner: TeamId.Neutral,
        contester: null,
        collected: [],
        looted: [],
        upgraded: [],
    }]
    for(const log of logs) {
        if(log[0] === 0) continue
        const prevState = states[states.length - 1]
        const timestamp = log[0]
        const actor = log[2]
        const amount = log[3]
        switch(log[1]) {
            case(CampEventId.StartCapture): {
                states.push({
                    ...prevState,
                    timestamp,
                    contester: actor
                })
                break;
            }
            case(CampEventId.Capture): {
                states.push({
                    ...prevState,
                    timestamp,
                    owner: actor,
                    contester: null,
                })
                break;
            }
            case(CampEventId.CancelCapture): {
                if(states[states.length - 1].contester === null) break;
                states.push({
                    ...prevState,
                    timestamp,
                    contester: null
                })
                break;
            }

            case(CampEventId.Collect): {
                states.push({
                    ...prevState,
                    timestamp,
                    collected: prevState.collected.concat([{
                        timestamp,
                        team: actor,
                        amount,
                    }])
                })
                break;
            }
            case(CampEventId.Loot): {
                states.push({
                    ...prevState,
                    timestamp,
                    looted: prevState.looted.concat([{
                        timestamp,
                        team: actor,
                        amount,
                    }]),
                    contester: null  // Note: little optimization not to double loot+cancel capture.
                })
                break;
            }
            case(CampEventId.Upgrade): {
                states.push({
                    ...prevState,
                    timestamp,
                    upgraded: prevState.upgraded.concat([{
                        timestamp,
                        team: actor,
                    }]),
                })
                break;
            }

            case(CampEventId.SetStockpile): {
                break;
            }
            case(CampEventId.SetStartingCollectCooldown): {
                break;
            }
        }
    }
    return states
}

const tsvToArray = async (url: string) => {
    const response = await fetch(url)
    const text = await response.text()
    const logs = text
        .split('\n')
        .slice(1)
        .map((line) => line.split('\t'))
        .map(([heartbeat, event, team, value]) => [Number(heartbeat), Number(event), team, Number(value)] as ExportRaw)
    const states = logToStates(logs)
    return states.reverse()
}

// Singleton here.
const data = {
    '1': (async () => tsvToArray(tsv1))(),
    '2': (async () => tsvToArray(tsv2))(),
    '3': (async () => tsvToArray(tsv3))(),
    '5': (async () => tsvToArray(tsv5))(),
    '6': (async () => tsvToArray(tsv6))(),
    '7': (async () => tsvToArray(tsv7))(),
    '8': (async () => tsvToArray(tsv8))(),
    '9': (async () => tsvToArray(tsv9))(),
    '10': (async () => tsvToArray(tsv10))(),
    '11': (async () => tsvToArray(tsv11))(),
    '13': (async () => tsvToArray(tsv13))(),
    '16': (async () => tsvToArray(tsv16))(),
    '19': (async () => tsvToArray(tsv19))(),
}
const getTimelapseData = async () => {
    return {
        '1': await data['1'],
        '2': await data['2'],
        '3': await data['3'],
        '4': await data['4'],
        '5': await data['5'],
        '6': await data['6'],
        '7': await data['7'],
        '8': await data['8'],
        '9': await data['9'],
        '10': await data['10'],
        '11': await data['11'],
        '12': await data['12'],
        '13': await data['13'],
        '14': await data['14'],
        '15': await data['15'],
        '16': await data['16'],
        '17': await data['17'],
        '18': await data['18'],
        '19': await data['19'],
        '22': await data['22'],
        '25': await data['25'],
    }
}

const COLOR = {
    gold: '#B88938',
    red: '#EB1E28',
    green: '#4EA653',
    purple: '#8551B8',
    blue: '#1A81EF',
    orange: '#F26418',
    pink: '#D83171',
    grey: '#808080',
}

const TEAM_COLORS: Record<TeamId, string> = {
    [TeamId.Neutral]: COLOR.grey,
    [TeamId.Snakes]: COLOR.green,
    [TeamId.Bears]: COLOR.purple,
    [TeamId.Lynx]: COLOR.gold,
}

const BUBBLE_TIMEOUT = 60 * 10
export const updateTimelapse = async (tick: number) => {
    const data = await getTimelapseData()
    for(const poi of POIs.getArray()) {
        const states = data[String(poi.getId()) as keyof typeof data] as CampState[]
        if(!states) continue
        const state = states.find((state) => state.timestamp <= tick)

        const lastBubble = state.collected.concat(state.looted).filter(({timestamp}) => tick - timestamp < BUBBLE_TIMEOUT).slice().pop() ?? null
        poi.setProperties({
            colors: [
                {color: TEAM_COLORS[state.owner], distance: 1, weight: state.contester ? 0.7 : 1},
                {color: TEAM_COLORS[state.contester ?? state.owner], distance: 1, weight: state.contester ? 0.3 : 0},
            ],
            bubble: !lastBubble ? null : {
                team: lastBubble.team,
                amount: lastBubble.amount,
                color: TEAM_COLORS[lastBubble.team],
                offset: (tick - lastBubble.timestamp),
                opacity: Math.max(0, 1 - (tick - lastBubble.timestamp) / BUBBLE_TIMEOUT),
            }
        } as Partial<PoiProperties>)
    }
}

document.getElementById('tick').addEventListener('change', (event) => {
    const tick = Number((event.target as HTMLInputElement).value)

    const tickDisplayElement = document.getElementById('tick-display') as HTMLDivElement
    tickDisplayElement.innerHTML = [
        `${Math.floor(tick / 60 / 60)}`.padStart(1, '0'),
        `${Math.floor(tick / 60 % 60)}`.padStart(2, '0'),
        `${Math.floor(tick % 60)}`.padStart(2, '0'),
    ].join(':')

    updateTimelapse(tick)
})
document.getElementById('tick').dispatchEvent(new Event('change'));

setInterval(() => {
    const speed = Number((document.getElementById('speed') as HTMLInputElement)?.value ?? 0)
    const tickElement = document.getElementById('tick') as HTMLInputElement
    const tick = Number(tickElement.value)
    const min = Number(tickElement.min)
    const max = Number(tickElement.max)

    if(!speed) return
    if(speed > 0 && tick === max) return
    if(speed < 0 && tick === min) return

    tickElement.value = String(Math.max(0, Math.min(max, tick + speed)))
    tickElement.dispatchEvent(new Event('change'));

    const tickDisplayElement = document.getElementById('tick-display') as HTMLDivElement
    tickDisplayElement.innerHTML = [
        `${Math.floor(tick / 60 / 60)}`.padStart(1, '0'),
        `${Math.floor(tick / 60 % 60)}`.padStart(2, '0'),
        `${Math.floor(tick % 60)}`.padStart(2, '0'),
    ].join(':')
}, 100)

document.getElementById('backward3').addEventListener('click', () => (document.getElementById('speed') as HTMLInputElement).value = '-60')
document.getElementById('backward2').addEventListener('click', () => (document.getElementById('speed') as HTMLInputElement).value = '-10')
document.getElementById('backward1').addEventListener('click', () => (document.getElementById('speed') as HTMLInputElement).value = '-1')
document.getElementById('pause'    ).addEventListener('click', () => (document.getElementById('speed') as HTMLInputElement).value = '0')
document.getElementById('forward1' ).addEventListener('click', () => (document.getElementById('speed') as HTMLInputElement).value = '1')
document.getElementById('forward2' ).addEventListener('click', () => (document.getElementById('speed') as HTMLInputElement).value = '10')
document.getElementById('forward3' ).addEventListener('click', () => (document.getElementById('speed') as HTMLInputElement).value = '60')
